function runMurasaki() {
    console.log("Running Murasaki");
    
    var murasakiWidth = 11;
    var murasakiHeight = 11;
    var imagePreviewPixelScale = 7;
    
    $(document).ready(function() {
        document.addEventListener('touchmove', function(e) { e.preventDefault(); }, false);
    });
    
    // Create a canvas element to downsample images
    var canvas = document.createElement('canvas');
    canvas.id = 'canvas';
    canvas.width = murasakiWidth;
    canvas.height = murasakiHeight;
    document.body.appendChild(canvas);
    
    // Set the gallery's idle event function
    var gallery = $('#imagegallery');
    gallery = gallery.data('galleria');
    if (gallery != null) {
        // Replace images in the gallery with ones in the photo library
        navigator.camera.getPicture(success, fail, {quality: 50, destinationType: navigator.camera.DestinationType.DATA_URL, sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY });
    }
    
    Galleria.on('image', function(event) {
        console.log("New image selected");
        var activeImage = this.getActiveImage();
        if (activeImage != null) {
            var canvas = $("#canvas")[0];
            var context = canvas.getContext("2d");
            context.drawImage(activeImage, 0, 0, canvas.width, canvas.height);

            var imagePreview = $("#imagepreview")[0];
            if (imagePreview != null) {
                imagePreview.src = canvas.toDataURL();
                var scaledWidth = murasakiWidth*imagePreviewPixelScale;
                var scaledHeight = murasakiHeight*imagePreviewPixelScale;
                imagePreview.style.width = scaledWidth.toString()+"px";
                imagePreview.style.height = scaledHeight.toString()+"px";
            }
        }
    });
    
    $("#submit").click(function( event ) {
        console.log("SUBMITTING");
        var imagePreview = $("#imagepreview")[0];
        if (imagePreview != null) {
            var imageBlob = imagePreview.src;
            $.get( "http://192.168.1.12:3000/", { imageData: imageBlob } )
                .done(function(data) {
                    console.log("Response: ", data);
                    var returnImg = document.createElement('img');
                    returnImg.src = data;
                    document.body.appendChild(returnImg);
            });
        }
    });
    
    $("#edit").click(function( event ) {
        console.log("EDITTING");
        var painterdiv = $("#painterdiv");
        var imagegallery = $("#imagegallery");
        if (painterdiv != null && imagegallery != null) {
            painterdiv.show();
            imagegallery.hide();
            createPainter();
        }
    });
}
    
function success(imageData) {
    console.log("SUCCESS");
    var gallery = $('#imagegallery');
    if (gallery != null) {
        gallery = gallery.data('galleria');
        gallery.load({ image: "data:image/jpeg;base64," + imageData });
    }
}
    
function fail(message) {
    console.log("FAIL");
}

// From:
// http://cssdeck.com/labs/jwvajze4/
function createPainter() {
	
    console.log("CREATING PAINTER");
	var canvas = document.querySelector('#painter');
	var ctx = canvas.getContext('2d');
	
	var sketch = document.querySelector('#painterdiv');
	var sketch_style = getComputedStyle(sketch);
	canvas.width = parseInt(sketch_style.getPropertyValue('width'));
	canvas.height = parseInt(sketch_style.getPropertyValue('height'));
	
	
	// draw image
	var img = new Image();
	img.src = 'http://cssdeck.com/uploads/media/items/3/3yiC6Yq.jpg';
	ctx.drawImage(img, 20, 20);
	
	// Determine Tool
	var tool = 'pencil';	
	
	// Creating a tmp canvas
	var tmp_canvas = document.createElement('canvas');
	var tmp_ctx = tmp_canvas.getContext('2d');
	tmp_canvas.id = 'tmp_canvas';
	tmp_canvas.width = canvas.width;
	tmp_canvas.height = canvas.height;
	
	sketch.appendChild(tmp_canvas);

	var mouse = {x: 0, y: 0};
	var last_mouse = {x: 0, y: 0};
	
	// Pencil Points
	var ppts = [];
	
	/* Mouse Capturing Work */
	tmp_canvas.addEventListener('mousemove', function(e) {
		mouse.x = typeof e.offsetX !== 'undefined' ? e.offsetX : e.layerX;
		mouse.y = typeof e.offsetY !== 'undefined' ? e.offsetY : e.layerY;
	}, false);
	
	canvas.addEventListener('mousemove', function(e) {
		mouse.x = typeof e.offsetX !== 'undefined' ? e.offsetX : e.layerX;
		mouse.y = typeof e.offsetY !== 'undefined' ? e.offsetY : e.layerY;
	}, false);
	
	
	/* Drawing on Paint App */
	tmp_ctx.lineWidth = 5;
	tmp_ctx.lineJoin = 'round';
	tmp_ctx.lineCap = 'round';
	tmp_ctx.strokeStyle = 'blue';
	tmp_ctx.fillStyle = 'blue';
	
	tmp_canvas.addEventListener('mousedown', function(e) {
		tmp_canvas.addEventListener('mousemove', onPaint, false);
		
		mouse.x = typeof e.offsetX !== 'undefined' ? e.offsetX : e.layerX;
		mouse.y = typeof e.offsetY !== 'undefined' ? e.offsetY : e.layerY;
		
		ppts.push({x: mouse.x, y: mouse.y});
		
		onPaint();
	}, false);
	
	tmp_canvas.addEventListener('mouseup', function() {
		tmp_canvas.removeEventListener('mousemove', onPaint, false);
		
		ctx.globalCompositeOperation = 'source-over';
		
		// Writing down to real canvas now
		ctx.drawImage(tmp_canvas, 0, 0);
		// Clearing tmp canvas
		tmp_ctx.clearRect(0, 0, tmp_canvas.width, tmp_canvas.height);
		
		// Emptying up Pencil Points
		ppts = [];
	}, false);
	
	var onPaint = function() {
		
        console.log("ON PAINT");
		// Saving all the points in an array
		ppts.push({x: mouse.x, y: mouse.y});
		
		if (ppts.length < 3) {
			var b = ppts[0];
			tmp_ctx.beginPath();
			//ctx.moveTo(b.x, b.y);
			//ctx.lineTo(b.x+50, b.y+50);
			tmp_ctx.arc(b.x, b.y, tmp_ctx.lineWidth / 2, 0, Math.PI * 2, !0);
			tmp_ctx.fill();
			tmp_ctx.closePath();
			
			return;
		}
		
		// Tmp canvas is always cleared up before drawing.
		tmp_ctx.clearRect(0, 0, tmp_canvas.width, tmp_canvas.height);
		
		tmp_ctx.beginPath();
		tmp_ctx.moveTo(ppts[0].x, ppts[0].y);
		
		for (var i = 1; i < ppts.length - 2; i++) {
			var c = (ppts[i].x + ppts[i + 1].x) / 2;
			var d = (ppts[i].y + ppts[i + 1].y) / 2;
			
			tmp_ctx.quadraticCurveTo(ppts[i].x, ppts[i].y, c, d);
		}
		
		// For the last 2 points
		tmp_ctx.quadraticCurveTo(
			ppts[i].x,
			ppts[i].y,
			ppts[i + 1].x,
			ppts[i + 1].y
		);
		tmp_ctx.stroke();
		
	};
}